<?php

namespace Torbor\yii\extensions;

use yii\base\Component;
use Zelenin\SmsRu\Api;
use Zelenin\SmsRu\Auth\ApiIdAuth;
use Zelenin\SmsRu\Auth\LoginPasswordAuth;
use Zelenin\SmsRu\Auth\LoginPasswordSecureAuth;
use Zelenin\SmsRu\Entity\Sms as SmsEntity;


class Sms extends Component
{
    /**
     * @var \Zelenin\SmsRu\Api
     */
    private $sms;

    public $api_id;
    public $login;
    public $password;

    public function init()
    {
        if (!empty($this->login) && !empty($this->password)) {
            $this->sms = (!empty($this->api_id))
                ? new Api(new LoginPasswordSecureAuth($this->login, $this->password, $this->api_id))
                : new Api(new LoginPasswordAuth($this->login, $this->password));
        } else {
            $this->sms = new Api(new ApiIdAuth($this->api_id));
        }

        parent::init();
    }

    /**
     * Отправить смс
     *
     * @param $phone
     * @param $text
     * @return \Zelenin\SmsRu\Response\SmsResponse
     */
    public function sms_send($phone, $text)
    {
        return $this->sms->smsSend(new SmsEntity($phone, $text));
    }

    /**
     * Узнать стоимость смс
     *
     * @param $phone
     * @param $text
     * @return \Zelenin\SmsRu\Response\SmsCostResponse
     */
    public function sms_cost($phone, $text)
    {
        return $this->sms->smsCost(new SmsEntity($phone, $text));
    }

    /**
     * @param string $name
     * @param array $parameters
     *
     * @return mixed
     */
    public function __call($name, $parameters)
    {
        return method_exists($this->sms, $name)
            ? call_user_func_array([$this->sms, $name], $parameters)
            : call_user_func_array([$this, $name], $parameters);
    }
}
